<?php
use MillionMail\Helpers\Facades\ChannelLog;

if (!function_exists('logMillionMail')) {
    function logMillionMail($message, array $context = [])
    {
        ChannelLog::info('info', $message, $context);
    }
}

if (!function_exists('logErrorSendMillionMail')) {
    function logErrorSendMillionMail($message, array $context = [])
    {
        ChannelLog::info('info', $message, $context);
    }
}

if (!function_exists('validateEmail')) {
    function validateEmail($value)
    {
        $__pattern = '(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,24}|museum|travel)';
        $__regex   = '/^[a-z0-9.!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@' . $__pattern . '$/i';

        if (preg_match($__regex, $value)) {
            return true;
        } else {
            return false;
        }
    }
}
<?php
namespace MillionMail\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class ChannelLog
 * @package MillionMail\Helpers\Facades
 */
class ChannelLog extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return "channellog";
    }
}
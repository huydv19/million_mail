- Migrations:

    -- add jobs table --    
    Run: php artisan queue:table
    Run: php artisan migrate

- Queue Connection: 
    add [MillionMailService::getQueueConnection()] to [connections] in config/queue.php file.
         
- Notes:
QUEUE_DRIVER=database (sẽ chạy queue trên console) 
không nên là QUEUE_DRIVER=sync (auto bắn mail)


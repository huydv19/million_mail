<?php

namespace MillionMail\Repositories;

use App\Repositories\Base\CustomRepository;
use MillionMail\Model\Entities\FailedEmails;

class FailedEmailsRepository extends CustomRepository
{
    const ERROR_LIMIT = '5';
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return FailedEmails::class;
    }

    public function getListFailedEmail() {
        return $this->search()->select('email')->where('count', '>', static::ERROR_LIMIT)->pluck('email')->toArray();
    }

    public function getEmails($email = [])
    {
        return $this->search()->all();
    }

    public function findByEmail($email)
    {
        return $this->firstOrNew(['email' => $email]);
    }
}
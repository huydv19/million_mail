<?php

namespace MillionMail;

use MillionMail\Repositories\FailedEmailsRepository;
use Illuminate\Support\Facades\Mail;

class MillionMailService
{
    protected $_failedEmailsRepository;

    const SEND_MAIL_PAGE_LIMIT = 3;
    const SEND_MAIL_GROUP_LIMIT = 2;
    const SEND_MAIL_CONNECTION_PREFIX = 'queue_';
    const SEND_MAIL_QUEUE_PREFIX = 'queue_';
    const SEND_MAIL_QUEUE_LIMIT = 5;
    const SEND_MAIL_TITLE_EMAIL_INVALID = 'Email is invalid: ';
    const SEND_MAIL_QUEUE_RETRY = 90;


   public function __construct(FailedEmailsRepository $failedEmailsRepository)
   {
       $this->_failedEmailsRepository = $failedEmailsRepository;
   }

    public function createQueue($userRepository)
    {
        try {
            $totalUsers = $userRepository->getTotalListAdminUser();
            $pageLimit = static::SEND_MAIL_PAGE_LIMIT;
            $totalPage = ceil($totalUsers / $pageLimit);
            $groupLimit = static::SEND_MAIL_GROUP_LIMIT;

            $connectionPrex = static::SEND_MAIL_CONNECTION_PREFIX;
            $queueNamePrex = static::SEND_MAIL_QUEUE_PREFIX;
            $queueLimit = static::SEND_MAIL_QUEUE_LIMIT;

            $currentQueue = 1;
            $startIdSendMail = 1;

            $listFailedEmails = $this->_failedEmailsRepository->getListFailedEmail();

            for ($page = 1; $page <= $totalPage; $page++) {
                // get email user by paging
                $listEmailUsersOrigin = $userRepository->getEmailsForSes($page, $pageLimit);
                $listEmailUsers = [];

                // Validate list emails py paging
                foreach ($listEmailUsersOrigin as $email) {
                    // If email is right format and is not belong to failed_emails table in database
                    if (validateEmail($email) && !in_array($email, $listFailedEmails)) {
                        array_push($listEmailUsers, $email);
                    } else {
                        logErrorSendMillionMail(trans('validation.email', ['attribute' => $email]));
                    }
                }

                // Devide list email by paging to packages
                $groups = array_chunk($listEmailUsers, $groupLimit);

                foreach ($groups as $group) {
                    if ($currentQueue > $queueLimit) {
                        $currentQueue = 1;
                    }
                    $connection = $connectionPrex . $currentQueue;
                    $queueName = $queueNamePrex . $currentQueue;

                    $millionMail = (new SendMillionMail())->onConnection($connection)->onQueue($queueName);
                    $currentQueue++;

                    Mail::to($group)->queue($millionMail);

                    // Log info send mail
                    $dataLogMail = [];
                    $dataLogMail[]['id'] = $startIdSendMail;
                    $startIdSendMail++;
                    $dataLogMail[]['page'] = $page;
                    $dataLogMail[]['connection'] = $connection;
                    $dataLogMail[]['queue'] = $queueName;
                    $dataLogMail[]['email'] = $group;
                    logMillionMail($dataLogMail);
                }
            }

        } catch (\Exception $e) {
            logError($e->getMessage());
        }
    }

    public static function getQueueConnection()
    {
        $result = [];
        for ($i  = 1; $i <= static::SEND_MAIL_QUEUE_LIMIT; $i++) {
            $result[static::SEND_MAIL_QUEUE_PREFIX . $i] = [
                'driver' => 'database',
                'table' => 'jobs',
                'queue' => static::SEND_MAIL_QUEUE_PREFIX . $i,
                'retry_after' => static::SEND_MAIL_QUEUE_RETRY,
            ];
        }
        return $result;
    }
}
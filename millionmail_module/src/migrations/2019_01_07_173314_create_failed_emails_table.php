<?php

use \App\Database\Migration\CustomBlueprint as Blueprint;

class CreateFailedEmailsTable extends \App\Database\Migration\Create
{
    protected $_table = 'failed_emails';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 256);
            $table->integer('count')->default(1);
            $table->actionBy();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_emails');
    }
}

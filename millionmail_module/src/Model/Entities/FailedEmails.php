<?php

namespace MillionMail\Model\Entities;

use App\Model\Base\Base;
use Illuminate\Notifications\Notifiable;

class FailedEmails extends Base
{
    protected $table = "failed_emails";
    use Notifiable;
    protected $_alias = 'failed_emails';
    protected $fillable = ['email', 'count'];
}


<?php

namespace MillionMail;

use App\Helpers\Facades\ChannelLog;
use MillionMail\Repositories\FailedEmailsRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Container\Container;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Mail\Mailer as MailerContract;
use Illuminate\Queue\SerializesModels;


class SendMillionMail extends Mailable
{
    use Queueable, SerializesModels;

    const SEND_MAIL_SUBJECT = 'Send mail to million users success';
    const SEND_MAIL_ERROR_LIMIT = 5;
    const SEND_MAIL_VIEW_TEMPLATE = 'backend.mail.millionMailTemplate';
    const SEND_MAIL_TITLE = 'backend.mail.millionMailTemplate';

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $viewData = [
            'title' => static::SEND_MAIL_TITLE,
        ];

        $result = $this
            ->view(static::SEND_MAIL_VIEW_TEMPLATE)
            ->subject(static::SEND_MAIL_SUBJECT)
            ->with($viewData);

        return $result;
    }

    public function send(MailerContract $mailer)
    {
        try {
            Container::getInstance()->call([$this, 'build']);

            $mailer->send($this->buildView(), $this->buildViewData(), function ($message) {
                $this->buildFrom($message)
                    ->buildRecipients($message)
                    ->buildSubject($message)
                    ->buildAttachments($message)
                    ->runCallbacks($message);
            });
        } catch (\Exception $e) {
            $error = $e->getMessage();
            $awsError = $e->getAwsErrorMessage();
            if ($error && !$awsError) {
                logErrorSendMillionMail($error);
            }
            if ($awsError) {
                logErrorSendMillionMail($awsError);
            }

            // Insert DB
            $emailError = [];
            $failedEmailEntity = app(FailedEmailsRepository::class);
            foreach ($emailError as $email) {
                $entity = $failedEmailEntity->findByEmail($email);
                if ($entity->exists && $entity->count < static::SEND_MAIL_ERROR_LIMIT + 1) {// update, count++
                    $entity->count+=1;
                }
                $entity->save();
            }
        }
        return $mailer;
    }

}
